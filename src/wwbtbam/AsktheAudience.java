package wwbtbam;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.border.LineBorder;
import javax.swing.SwingConstants;
import javax.swing.JTextArea;

public class AsktheAudience extends JFrame {
	private JLabel lblBackground;
	private JPanel panelheader;
	private JLabel lbltopic;
	private JLabel lbloverview;
	private JPanel panelbodytopic;
	private JButton btnwalkaway;
	private JLabel lbltopiclist;

	/**
	 * Create the frame.
	 */
	public AsktheAudience() {
		super();
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JPanel panel = new JPanel();
		panel.setBounds(0, 0, 1091, 663);
		contentPane.add(panel);
		panel.setLayout(null);
		
		panelheader = new JPanel();
		panelheader.setOpaque(false);
		panelheader.setBorder(new LineBorder(new Color(102, 205, 170), 10));
		panelheader.setBounds(-16, 35, 1156, 126);
		panel.add(panelheader);
		panelheader.setLayout(null);
		
		lbltopic = new JLabel("TOPICS");
		lbltopic.setHorizontalAlignment(SwingConstants.CENTER);
		lbltopic.setForeground(new Color(255, 255, 255));
		lbltopic.setFont(new Font("Georgia", Font.PLAIN, 50));
		lbltopic.setBounds(303, 15, 534, 89);
		panelheader.add(lbltopic);
		
		panelbodytopic = new JPanel();
		panelbodytopic.setOpaque(false);
		panelbodytopic.setBounds(10, 167, 1071, 465);
		panel.add(panelbodytopic);
		panelbodytopic.setLayout(null);
		
		lbloverview = new JLabel("Overview of ICT");
		lbloverview.setBounds(365, 28, 332, 35);
		panelbodytopic.add(lbloverview);
		lbloverview.setHorizontalAlignment(SwingConstants.CENTER);
		lbloverview.setForeground(Color.WHITE);
		lbloverview.setFont(new Font("Georgia", Font.PLAIN, 35));
		
		lbltopiclist = new JLabel("<html>1.   Introduction to Computers<br><br>"
				+"2.   Elements of Computer System<br><br>"
				+ "3.   Classification of Computers<br><br>"
				+ "4.   Capabilities and Limitation of Computers<br><br>"
				+ "5.   History of Computing</html>");
		lbltopiclist.setForeground(new Color(255, 250, 250));
		lbltopiclist.setFont(new Font("Georgia", Font.PLAIN, 20));
		lbltopiclist.setBounds(354, 92, 425, 278);
		panelbodytopic.add(lbltopiclist);
		
		btnwalkaway = new JButton("");
		btnwalkaway.setHorizontalTextPosition(SwingConstants.CENTER);
		btnwalkaway.setForeground(Color.WHITE);
		btnwalkaway.setFont(new Font("Georgia", Font.BOLD, 15));
		btnwalkaway.setContentAreaFilled(false);
		btnwalkaway.setBorderPainted(false);
		btnwalkaway.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_35_3877677090.png")));
		btnwalkaway.setBounds(39, 30, 89, 59);
		panelheader.add(btnwalkaway);
		
		
		
		lblBackground = new JLabel("");
		lblBackground.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_20_4528081040.png")));
		
		lblBackground.setBounds(0, -66, 1091, 729);
		panel.add(lblBackground);
		
		
		
		
		setResizable(false);
		setTitle("Who wants to be a millionaire ICT Edition");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(170, 20, 1097, 692);
		setVisible(true);
	}
}
