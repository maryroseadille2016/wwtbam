package wwbtbam;

import java.awt.Color;
import java.awt.Component;
import java.awt.EventQueue;
import java.awt.Image;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import net.miginfocom.swing.MigLayout;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Font;

public class WelcomeScreen extends JFrame {


	private JButton btnInstruct;
	private JButton btnDevelopers;
	private JButton btnEnd;
	private JLabel lblNewLabel;
	private JLabel lblBackground;
	private JButton BtnStart;
	private JPanel panelheader;
	private JLabel lblhowtoplay;
	private JButton btnarrowleft;
	private JButton btnarrowright;
	private JLabel lblLifeLine;
	private JLabel lbl5050;
	private JPanel panelbody;
	private JLabel lblcall;
	private JLabel lblask;
	private JLabel lblcalldesc;
	private JLabel lblaskdesc;
	private JLabel lbl5050desc;
	private JButton btnwalkaway;
	private JPanel panelbody2;
	private JPanel panel;
	private JLabel lblgameplaydesc;
	private int ctrarrow;
	private JLabel lblwalkawaypic;
	private JLabel lblWalkAway;
	private JLabel lblwalkawaydesc;
	private JPanel panelbody3;
	private JLabel lblsafetydesc;
	private JLabel lblmoneytable;
	private JLabel lblSafetynet;
	private JLabel lbltopic;
	private JPanel panelbodytopic;
	private JLabel lbloverview;
	private JLabel lbltopiclist;

	/**
	 * Create the frame.
	 */
	public WelcomeScreen() {
		super();
		
		JPanel contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 1091, 663);
		contentPane.add(panel);
		panel.setLayout(null);
		
		BtnStart = new JButton("Start Game");
		BtnStart.setForeground(Color.WHITE);
		BtnStart.setFont(new Font("Georgia", Font.BOLD, 15));
		BtnStart.setHorizontalTextPosition(AbstractButton.CENTER);
		BtnStart.setOpaque(false);
		BtnStart.setContentAreaFilled(false);
		BtnStart.setBorderPainted(false);
		BtnStart.setIcon(new ImageIcon(WelcomeScreen.class.getResource("/images/imageedit_3_9138455551.png")));
		BtnStart.setBounds(435, 484, 258, 45);
		panel.add(BtnStart);
		
		
		btnInstruct = new JButton("Instructions");
		btnInstruct.setForeground(Color.WHITE);
		btnInstruct.setFont(new Font("Georgia", Font.BOLD, 15));
		btnInstruct.setHorizontalTextPosition(AbstractButton.CENTER);
		btnInstruct.setOpaque(false);
		btnInstruct.setContentAreaFilled(false);
		btnInstruct.setBorderPainted(false);
		btnInstruct.setIcon(new ImageIcon(WelcomeScreen.class.getResource("/images/imageedit_3_9138455551.png")));
		btnInstruct.setBounds(169, 569, 258, 52);
		panel.add(btnInstruct);
		btnInstruct.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				panelheader.setVisible(true);
				btnarrowleft.setVisible(true);
				btnarrowright.setVisible(true);
				panelbody.setVisible(true);
				
				BtnStart.setVisible(false);
				btnInstruct.setVisible(false);
				btnDevelopers.setVisible(false);
				btnEnd.setVisible(false);
				lblNewLabel.setVisible(false);
				
				
				btnarrowright.setEnabled(true);
				btnarrowleft.setEnabled(false);
				lblhowtoplay.setText("HOW TO PLAY");
				ctrarrow=1;
			}

		});
		
		btnDevelopers = new JButton("Topics");
		btnDevelopers.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				panelbodytopic.setVisible(true);
				BtnStart.setVisible(false);
				btnInstruct.setVisible(false);
				btnDevelopers.setVisible(false);
				btnEnd.setVisible(false);
				lblNewLabel.setVisible(false);
				panelheader.setVisible(true);
				lblhowtoplay.setText("TOPIC");
			}
		});
		btnDevelopers.setForeground(Color.WHITE);
		btnDevelopers.setFont(new Font("Georgia", Font.BOLD, 15));
		btnDevelopers.setHorizontalTextPosition(AbstractButton.CENTER);
		btnDevelopers.setOpaque(false);
		btnDevelopers.setContentAreaFilled(false);
		btnDevelopers.setBorderPainted(false);
		btnDevelopers.setIcon(new ImageIcon(WelcomeScreen.class.getResource("/images/imageedit_3_9138455551.png")));
		btnDevelopers.setBounds(435, 576, 269, 38);
		panel.add(btnDevelopers);
		
		btnEnd = new JButton("End Game");
		btnEnd.setForeground(Color.WHITE);
		btnEnd.setFont(new Font("Georgia", Font.BOLD, 15));
		btnEnd.setHorizontalTextPosition(AbstractButton.CENTER);
		btnEnd.setOpaque(false);
		btnEnd.setContentAreaFilled(false);
		btnEnd.setBorderPainted(false);
		btnEnd.setIcon(new ImageIcon(WelcomeScreen.class.getResource("/images/imageedit_3_9138455551.png")));
		btnEnd.setBounds(714, 576, 273, 38);
		panel.add(btnEnd);
		btnEnd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			
				System.exit(0);
			}

		});
		
		
		lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(WelcomeScreen.class.getResource("/images/imageedit_6_9889434751.png")));
		lblNewLabel.setBounds(251, 65, 603, 375);
		panel.add(lblNewLabel);
		
		
		
		BtnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new GameFrame();
			}
		});
		
		
		/*begin Instruct*/
		
		panelheader = new JPanel();
		panelheader.setOpaque(false);
		panelheader.setBorder(new LineBorder(new Color(102, 205, 170), 10));
		panelheader.setBounds(-16, 35, 1156, 126);
		panel.add(panelheader);
		panelheader.setLayout(null);
		
		lblhowtoplay = new JLabel("HOW TO PLAY");
		lblhowtoplay.setHorizontalAlignment(SwingConstants.CENTER);
		lblhowtoplay.setForeground(new Color(255, 255, 255));
		lblhowtoplay.setFont(new Font("Georgia", Font.PLAIN, 50));
		lblhowtoplay.setBounds(303, 15, 534, 89);
		panelheader.add(lblhowtoplay);
		
		btnwalkaway = new JButton("");
		btnwalkaway.setHorizontalTextPosition(SwingConstants.CENTER);
		btnwalkaway.setForeground(Color.WHITE);
		btnwalkaway.setFont(new Font("Georgia", Font.BOLD, 15));
		btnwalkaway.setContentAreaFilled(false);
		btnwalkaway.setBorderPainted(false);
		btnwalkaway.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_35_3877677090.png")));
		btnwalkaway.setBounds(39, 30, 89, 59);
		panelheader.add(btnwalkaway);
		btnwalkaway.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ctrarrow=1;
				panelheader.setVisible(false);
				btnarrowleft.setVisible(false);
				btnarrowright.setVisible(false);
				panelbody.setVisible(false);
				panelbody2.setVisible(false);
				panelbody3.setVisible(false);
				panelbodytopic.setVisible(false);
				
				BtnStart.setVisible(true);
				btnInstruct.setVisible(true);
				btnDevelopers.setVisible(true);
				btnEnd.setVisible(true);
				lblNewLabel.setVisible(true);
			}

		});
		
		
		btnarrowleft = new JButton("");
		btnarrowleft.setForeground(Color.WHITE);
		btnarrowleft.setFont(new Font("Georgia", Font.BOLD, 15));
		btnarrowleft.setHorizontalTextPosition(AbstractButton.CENTER);
		btnarrowleft.setOpaque(false);
		btnarrowleft.setContentAreaFilled(false);
		btnarrowleft.setBorderPainted(false);
		btnarrowleft.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_1_4955913685.png")));
		btnarrowleft.setBounds(19, 513, 101, 107);
		panel.add(btnarrowleft);
		btnarrowleft.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(ctrarrow==1){
					
				}else if(ctrarrow==2){
					panelbody2.setVisible(false);
					panelbody.setVisible(true);
					ctrarrow=1;
					btnarrowleft.setEnabled(false);
					//System.out.println(ctrarrow);
				}else if(ctrarrow==3){
					panelbody3.setVisible(false);
					panelbody2.setVisible(true);
					btnarrowright.setEnabled(true);
					ctrarrow=2;
					//System.out.println(ctrarrow);
				}
				
			}
		});
		
		btnarrowright = new JButton("");
		btnarrowright.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(ctrarrow==1){
					btnarrowleft.setEnabled(true);
					panelbody2.setVisible(true);
					panelbody.setVisible(false);
					ctrarrow=2;
					//System.out.println(ctrarrow);
					
					
				}else if (ctrarrow==2){
					panelbody3.setVisible(true);
					panelbody2.setVisible(false);
					btnarrowright.setEnabled(false);
					ctrarrow=3;
					//System.out.println(ctrarrow);
					
				}
				
			}
		});
		btnarrowright.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_5_6436113733.png")));
		btnarrowright.setOpaque(false);
		btnarrowright.setHorizontalTextPosition(SwingConstants.CENTER);
		btnarrowright.setForeground(Color.WHITE);
		btnarrowright.setFont(new Font("Georgia", Font.BOLD, 15));
		btnarrowright.setContentAreaFilled(false);
		btnarrowright.setBorderPainted(false);
		btnarrowright.setBounds(970, 513, 101, 107);
		panel.add(btnarrowright);
		
		panelbody = new JPanel();
		panelbody.setOpaque(false);
		panelbody.setBounds(129, 167, 852, 465);
		panel.add(panelbody);
		panelbody.setLayout(null);
		
		lblLifeLine = new JLabel("Life Line");
		lblLifeLine.setBounds(370, 11, 119, 35);
		panelbody.add(lblLifeLine);
		lblLifeLine.setHorizontalAlignment(SwingConstants.CENTER);
		lblLifeLine.setForeground(Color.WHITE);
		lblLifeLine.setFont(new Font("Georgia", Font.PLAIN, 30));
		
		lbl5050 = new JLabel("");
		lbl5050.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_27_7266052596.png")));
		lbl5050.setBounds(85, 51, 80, 57);
		panelbody.add(lbl5050);
		
		lblcall = new JLabel("");
		lblcall.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_30_6104888644.png")));
		lblcall.setBounds(391, 51, 80, 57);
		panelbody.add(lblcall);
		
		lblask = new JLabel("");
		lblask.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_33_9034619878.png")));
		lblask.setBounds(635, 52, 80, 57);
		panelbody.add(lblask);
		
		lblcalldesc = new JLabel("<html>The User is given one (1) opportunity to call a friends and reads them the question and answer choices. "
				+ "The Contestant will receive the answer of his/her friend. The Contestant then has the choice of selecting "
				+ "an answer, using another available lifeline, or quitting the Game by stating he/she is walking away.</html>");
		lblcalldesc.setForeground(new Color(255, 255, 255));
		lblcalldesc.setFont(new Font("Georgia", Font.PLAIN, 15));
		lblcalldesc.setBounds(293, 119, 269, 178);
		panelbody.add(lblcalldesc);
		
		lblaskdesc = new JLabel("<html>The user is given one (1) opportunity to ask the audience which answer to a game question they believe is correct. "
				+ "The User will receive the results of the audience vote. The User then has the choice of selecting "
				+ "an answer, using another available lifeline, or quitting the Game by stating he/she is walking away.</html>");
		lblaskdesc.setFont(new Font("Georgia", Font.PLAIN, 15));
		lblaskdesc.setForeground(new Color(255, 255, 255));
		
		lblaskdesc.setBounds(584, 123, 248, 170);
		panelbody.add(lblaskdesc);
		
		lbl5050desc = new JLabel("<html>The Contestant will have one (1) �Fifty-Fifty� lifeline, whereby two (2) of the possible answer choices are eliminated. Following use of this lifeline, two (2) answer choices will remain, one (1) of which is the most correct answer choice. The Contestant then has the "
				+ "choice of selecting one of the two remaining answers, using another available lifeline, or quitting the Game.</html>");
		lbl5050desc.setHorizontalAlignment(SwingConstants.RIGHT);
		lbl5050desc.setVerticalTextPosition(SwingConstants.TOP);
		lbl5050desc.setForeground(Color.WHITE);
		lbl5050desc.setFont(new Font("Georgia", Font.PLAIN, 15));
		lbl5050desc.setBounds(10, 119, 259, 219);
		panelbody.add(lbl5050desc);
		panelbody2 = new JPanel();
		panelbody2.setOpaque(false);
		panelbody2.setBounds(129, 167, 852, 465);
		panel.add(panelbody2);
		panelbody2.setLayout(null);
		
		lblLifeLine = new JLabel("Game Play");
		lblLifeLine.setBounds(261, 11, 332, 35);
		panelbody2.add(lblLifeLine);
		lblLifeLine.setHorizontalAlignment(SwingConstants.CENTER);
		lblLifeLine.setForeground(Color.WHITE);
		lblLifeLine.setFont(new Font("Georgia", Font.PLAIN, 30));
		
		lblgameplaydesc = new JLabel("<html>All users will be asked up to fifteen (15) questions which are only related to the introduction of ICT. "
				+ "A user must choose the most correct answer choice of the four (4) possible answer choices provided. <br><br>"
				+ "For each question answered correctly, the stage advances. The questions and their level of difficulty are randomized. "
				+ "Which means the very first question they could get could be the hardest question in the game. <br><br>"
				+ "Each question has an assigned peso value that increases in amount from One Thousand pesos (P1,000) to Two Million Pesos (P2,000,000) "
				+ "in accordance with the prize table set. <br><br>"
				+ "A user may end his/her Game using walk away button and walk away with the winnings already earned. <br><br>"
				+ "If a user answers all fourteen (14) questions correctly, he/she wins Two Million Pesos (P2,000,000). <br><br>"
				+ "If a user answers a question incorrectly, he/she will be eliminated from the Game and will win the amount of the last �Safety Net� "
				+ "reached.</html>");
		lblgameplaydesc.setForeground(new Color(255, 250, 250));
		lblgameplaydesc.setFont(new Font("Georgia", Font.PLAIN, 17));
		lblgameplaydesc.setVerticalAlignment(SwingConstants.TOP);
		lblgameplaydesc.setBounds(62, 62, 721, 393);
		panelbody2.add(lblgameplaydesc);
		
		
		
		
		/*end Instruct*/
		
		/*begin safety net instruct*/
		panelbody3 = new JPanel();
		panelbody3.setOpaque(false);
		panelbody3.setBounds(129, 167, 852, 465);
		panel.add(panelbody3);
		panelbody3.setLayout(null);
		
		lblSafetynet = new JLabel("Safety Nets");
		lblSafetynet.setBounds(163, 0, 332, 35);
		panelbody3.add(lblSafetynet);
		lblSafetynet.setHorizontalAlignment(SwingConstants.CENTER);
		lblSafetynet.setForeground(Color.WHITE);
		lblSafetynet.setFont(new Font("Georgia", Font.PLAIN, 30));
		
		lblsafetydesc = new JLabel("<html>There are two �Safety Nets� that the User can acquire during the Game. <br>"
				+ "If the User answers the fifth (5th) question correctly (marked as yellow font color in prize table), "
				+ "he/she cannot leave with less than 20 Thousand Pesos (P20,000) upon giving a subsequent incorrect answer to questions six "
				+ "(6) through, and including, ten (10). <br>"
				+ "If the User answers the tenth (10th) question correctly, he/she cannot leave with less than "
				+ "One Hundred Fifty Thousand Pesos (P150,000) upon giving a subsequent incorrect answer. <br>"
				+ "If a User answers any of questions one (1) through five (5) incorrectly, the Game ends and the User leaves with nothing.</html>");
		lblsafetydesc.setForeground(new Color(255, 250, 250));
		lblsafetydesc.setFont(new Font("Georgia", Font.PLAIN, 16));
		lblsafetydesc.setVerticalAlignment(SwingConstants.TOP);
		lblsafetydesc.setBounds(62, 46, 555, 195);
		panelbody3.add(lblsafetydesc);
		
		lblmoneytable = new JLabel("");
		lblmoneytable.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_7_7191798288.png")));
		lblmoneytable.setBounds(627, 46, 200, 385);
		panelbody3.add(lblmoneytable);
		
		lblwalkawaydesc = new JLabel("<html>If the User chooses to walk away at any time, he/she will receive the prize money won to that point. "
				+ "For example, if the Contestant has correctly answered the question worth Ten Thousand Pesos (P10,000) "
				+ "and chooses to walk away instead of answering the question valued at Twenty Thousand Pesos (P20,000), "
				+ "he/she will receive Ten Thousand Pesos (P10,000).</html>");
		lblwalkawaydesc.setForeground(new Color(255, 250, 250));
		lblwalkawaydesc.setFont(new Font("Georgia", Font.PLAIN, 17));
		lblwalkawaydesc.setBounds(62, 297, 543, 134);
		panelbody3.add(lblwalkawaydesc);
		
		lblWalkAway = new JLabel("Walk Away");
		lblWalkAway.setHorizontalAlignment(SwingConstants.CENTER);
		lblWalkAway.setForeground(Color.WHITE);
		lblWalkAway.setFont(new Font("Georgia", Font.PLAIN, 30));
		lblWalkAway.setBounds(163, 252, 332, 35);
		panelbody3.add(lblWalkAway);
		
		lblwalkawaypic = new JLabel("");
		lblwalkawaypic.setIcon(new ImageIcon(AsktheAudience.class.getResource("/images/imageedit_35_3877677090.png")));
		lblwalkawaypic.setBounds(449, 240, 87, 50);
		panelbody3.add(lblwalkawaypic);
		/*end safety net instruct*/
		
		/*begin topic*/

		
		panelbodytopic = new JPanel();
		panelbodytopic.setOpaque(false);
		panelbodytopic.setBounds(10, 167, 1071, 465);
		panel.add(panelbodytopic);
		panelbodytopic.setLayout(null);
		
		lbloverview = new JLabel("Overview of ICT");
		lbloverview.setBounds(365, 28, 332, 35);
		panelbodytopic.add(lbloverview);
		lbloverview.setHorizontalAlignment(SwingConstants.CENTER);
		lbloverview.setForeground(Color.WHITE);
		lbloverview.setFont(new Font("Georgia", Font.PLAIN, 35));
		
		lbltopiclist = new JLabel("<html>1.   Introduction to Computers<br><br>"
				+"2.   Elements of Computer System<br><br>"
				+ "3.   Classification of Computers<br><br>"
				+ "4.   Capabilities and Limitation of Computers<br><br>"
				+ "5.   History of Computing</html>");
		lbltopiclist.setForeground(new Color(255, 250, 250));
		lbltopiclist.setFont(new Font("Georgia", Font.PLAIN, 20));
		lbltopiclist.setBounds(354, 92, 425, 278);
		panelbodytopic.add(lbltopiclist);
		lblBackground = new JLabel("");
		lblBackground.setIcon(new ImageIcon(WelcomeScreen.class.getResource("/images/imageedit_20_4528081040.png")));
		
		lblBackground.setBounds(0, -66, 1091, 729);
		panel.add(lblBackground);
		/*end topic*/
		
		panelbodytopic.setVisible(false);
		panelheader.setVisible(false);
		btnarrowleft.setVisible(false);
		btnarrowright.setVisible(false);
		panelbody.setVisible(false);
		panelbody2.setVisible(false);
		panelbody3.setVisible(false);
		
		setResizable(false);
		setTitle("Who wants to be a millionaire ICT Edition");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(170, 20, 1097, 692);
		setVisible(true);
		
	}


}
