package wwbtbam;


import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.AbstractButton;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.border.LineBorder;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.Font;
import javax.swing.JButton;
import javax.swing.SwingConstants;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

import javax.swing.JProgressBar;


public class GameFrame extends JFrame {
	Connect c;
	private JPanel contentPane;
	
	private JPanel panel;

	private JPanel panel_1;

	private JLabel lblMoney15;
	private JLabel lblMoney14;
	private JLabel lblMoney13;
	private JLabel lblMoney12;
	private JLabel lblMoney11;
	private JLabel lblMoney10;
	private JLabel lblMoney9;
	private JLabel lblMoney8;
	private JLabel lblMoney7;
	private JLabel lblMoney6;
	private JLabel lblMoney5;
	private JLabel lblMoney4;
	private JLabel lblMoney3;
	private JLabel lblMoney2;
	private JLabel lblMoney1;
	private JPanel panelQuestion;
	private JTextArea lblQuestion;
	private JPanel panelChoices;
	private JButton btnA;
	private JButton btnB;
	private JButton btnC;
	private JButton btnD;
	private JPanel panel_2;
	private JButton btnLL5050;
	private JButton btnLLCall;
	private JButton btnLLAskAudience;
	private JLabel labelGameBackground;
	String line;



	private ArrayList<Integer> list;
	private ArrayList<String> listchoices;
	private String CorrectAns;
	private String AnotherAns;
	private JPanel AtaPanel;
	private JPanel panel_5;
	private JLabel percentA;
	private JLabel percentB;
	private JLabel percentC;
	private JLabel percentD;
	private JPanel panel_6;
	private JProgressBar progressBarD;
	private JProgressBar progressBarC;
	private JProgressBar progressBarB;
	private JProgressBar progressBarA;
	private JPanel panel_4;
	private JLabel ChoiceA;
	private JLabel ChoiceB;
	private JLabel ChoiceC;
	private JLabel ChoiceD;
	protected ArrayList<Integer> percentlist;
	protected ArrayList<Integer> percentvalue;
	private JPanel cafpanel;
	private JLabel label;
	private JLabel label_1;
	private JLabel label_2;
	private JTextArea CallquestiontextArea;
	private JLabel callA;
	private JLabel callB;
	private JLabel callC;
	private JLabel callD;
	private JLabel callanslabel;
	private String currentquestion;
	private int moneyctr = 0;
	private int moneybank = 0;
	private int endctr = 0;
	private int accmoney;
	private JLabel lblquestionno;
	private String questionno;

	
	public GameFrame() {
		
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		panel = new JPanel();
		panel.setBounds(0, 0, 1091, 663);
		contentPane.add(panel);
		panel.setLayout(null);
		
		panel_1 = new JPanel();
		panel_1.setForeground(Color.BLACK);
		panel_1.setBorder(new LineBorder(SystemColor.activeCaption, 10, true));
		panel_1.setBounds(827, 11, 254, 641);
		panel.add(panel_1);
		panel_1.setBackground(new Color(70, 130, 180));
		panel_1.setLayout(null);
		
		lblMoney15 = new JLabel("          15     P 2,000,000");
		lblMoney15.setHorizontalAlignment(SwingConstants.LEFT);
		lblMoney15.setOpaque(true);
		lblMoney15.setBackground(new Color(70, 130, 180));
		lblMoney15.setForeground(Color.YELLOW);
		lblMoney15.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney15.setBounds(10, 14, 234, 40);
		panel_1.add(lblMoney15);
		
		 lblMoney14 = new JLabel("          14     P 1,000,000");
		 lblMoney14.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney14.setOpaque(true);
		lblMoney14.setBackground(new Color(70, 130, 180));
		lblMoney14.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney14.setForeground(Color.WHITE);
		lblMoney14.setBounds(10, 54, 234, 40);
		panel_1.add(lblMoney14);
		
		 lblMoney13 = new JLabel("          13     P 600,000");
		 lblMoney13.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney13.setHorizontalTextPosition(SwingConstants.CENTER);
		 lblMoney13.setBackground(new Color(70, 130, 180));
		 lblMoney13.setOpaque(true);
		lblMoney13.setForeground(Color.WHITE);
		lblMoney13.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney13.setBounds(10, 95, 234, 40);
		panel_1.add(lblMoney13);
		
		 lblMoney12 = new JLabel("          12     P 400,000");
		 lblMoney12.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney12.setBackground(new Color(70, 130, 180));
		 lblMoney12.setOpaque(true);
		lblMoney12.setForeground(Color.WHITE);
		lblMoney12.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney12.setBounds(10, 135, 234, 40);
		panel_1.add(lblMoney12);
		
		 lblMoney11 = new JLabel("          11     P 250,000");
		 lblMoney11.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney11.setBackground(new Color(70, 130, 180));
		 lblMoney11.setOpaque(true);
		lblMoney11.setForeground(Color.WHITE);
		lblMoney11.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney11.setBounds(10, 174, 234, 40);
		panel_1.add(lblMoney11);
		
		 lblMoney10 = new JLabel("          10     P 150,000");
		 lblMoney10.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney10.setBackground(new Color(70, 130, 180));
		 lblMoney10.setOpaque(true);
		lblMoney10.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney10.setForeground(Color.YELLOW);
		lblMoney10.setBounds(10, 214, 234, 40);
		panel_1.add(lblMoney10);
		
		 lblMoney9 = new JLabel("            9     P 100,000");
		 lblMoney9.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney9.setBackground(new Color(70, 130, 180));
		 lblMoney9.setOpaque(true);
		lblMoney9.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney9.setForeground(new Color(255, 255, 255));
		lblMoney9.setBounds(10, 254, 234, 43);
		panel_1.add(lblMoney9);
		
		 lblMoney8 = new JLabel("            8     P 70,000");
		 lblMoney8.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney8.setBackground(new Color(70, 130, 180));
		 lblMoney8.setOpaque(true);
		lblMoney8.setForeground(Color.WHITE);
		lblMoney8.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney8.setBounds(10, 297, 234, 40);
		panel_1.add(lblMoney8);
		
		 lblMoney7 = new JLabel("            7     P 50,000");
		 lblMoney7.setHorizontalAlignment(SwingConstants.LEFT);
		 lblMoney7.setBackground(new Color(70, 130, 180));
		 lblMoney7.setOpaque(true);
		lblMoney7.setForeground(Color.WHITE);
		lblMoney7.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney7.setBounds(10, 337, 234, 40);
		panel_1.add(lblMoney7);
		
		lblMoney6 = new JLabel("            6     P 35,000");
		lblMoney6.setHorizontalAlignment(SwingConstants.LEFT);
		lblMoney6.setBackground(new Color(70, 130, 180));
		lblMoney6.setOpaque(true);
		lblMoney6.setForeground(Color.WHITE);
		lblMoney6.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney6.setBounds(10, 376, 234, 40);
		panel_1.add(lblMoney6);
		
		lblMoney5 = new JLabel("            5     P 20,000");
		lblMoney5.setHorizontalAlignment(SwingConstants.LEFT);
		lblMoney5.setBackground(new Color(70, 130, 180));
		lblMoney5.setOpaque(true);
		lblMoney5.setForeground(Color.YELLOW);
		lblMoney5.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney5.setBounds(10, 417, 234, 40);
		panel_1.add(lblMoney5);
		
		lblMoney4 = new JLabel("            4     P 10,000");
		lblMoney4.setBackground(new Color(70, 130, 180));
		lblMoney4.setOpaque(true);
		lblMoney4.setForeground(Color.WHITE);
		lblMoney4.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney4.setBounds(10, 457, 234, 40);
		panel_1.add(lblMoney4);
		
		lblMoney3 = new JLabel("            3     P 5,000");
		lblMoney3.setBackground(new Color(70, 130, 180));
		lblMoney3.setOpaque(true);
		lblMoney3.setForeground(Color.WHITE);
		lblMoney3.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney3.setBounds(10, 497, 234, 40);
		panel_1.add(lblMoney3);
		
		lblMoney2 = new JLabel("            2     P 3,000");
		lblMoney2.setBackground(new Color(70, 130, 180));
		lblMoney2.setOpaque(true);
		lblMoney2.setForeground(Color.WHITE);
		lblMoney2.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney2.setBounds(10, 538, 234, 40);
		panel_1.add(lblMoney2);
		
		lblMoney1 = new JLabel("            1     P 1,000");
		lblMoney1.setOpaque(true);
		lblMoney1.setBackground(new Color(70, 130, 180));
		lblMoney1.setForeground(Color.WHITE);
		lblMoney1.setFont(new Font("Georgia", Font.PLAIN, 20));
		lblMoney1.setBounds(10, 578, 234, 43);
		panel_1.add(lblMoney1);
		
		 panelQuestion = new JPanel();
		panelQuestion.setBackground(new Color(70, 130, 180,200));
		panelQuestion.setBounds(20, 279, 797, 212);
		panel.add(panelQuestion);
		panelQuestion.setLayout(null);
		
		 lblQuestion = new JTextArea();
		 lblQuestion.setEditable(false);
		lblQuestion.setForeground(new Color(255, 255, 255));

		lblQuestion.setFont(new Font("Georgia", Font.PLAIN, 23));
		lblQuestion.setBounds(36, 36, 715, 133);
		panelQuestion.add(lblQuestion);
		
		 panelChoices = new JPanel();
		panelChoices.setBackground(new Color(70, 130, 180,110));
		panelChoices.setBounds(20, 502, 797, 150);
		panel.add(panelChoices);
		panelChoices.setLayout(null);
		
		 btnA = new JButton("A.     ");
		 btnA.setRolloverEnabled(false);
		 btnA.setForeground(Color.WHITE);
		 btnA.setFont(new Font("Georgia", Font.PLAIN, 15));
		 btnA.setHorizontalTextPosition(AbstractButton.CENTER);
		 btnA.setOpaque(false);
		 btnA.setContentAreaFilled(false);
		 btnA.setBorderPainted(false);
		 btnA.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_15_3035174943.png")));
		
		btnA.setBounds(27, 15, 360, 50);
		panelChoices.add(btnA);
		
		 btnB = new JButton("B.     ");
		 btnB.setRolloverEnabled(false);
		 btnB.setForeground(Color.WHITE);
		 btnB.setHorizontalTextPosition(AbstractButton.CENTER);
		 btnB.setOpaque(false);
		 btnB.setContentAreaFilled(false);
		 btnB.setBorderPainted(false);
		 btnB.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_20_4232220064.png")));
		 btnB.setFont(new Font("Georgia", Font.PLAIN, 15));
		btnB.setBounds(411, 15, 361, 50);
		panelChoices.add(btnB);
		
		 btnC = new JButton("C.     ");
		 btnC.setRolloverEnabled(false);
		 btnC.setForeground(Color.WHITE);
		 btnC.setHorizontalTextPosition(AbstractButton.CENTER);
		 btnC.setOpaque(false);
		 btnC.setContentAreaFilled(false);
		 btnC.setBorderPainted(false);
		 btnC.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_24_7123385619.png")));
		 btnC.setFont(new Font("Georgia", Font.PLAIN, 15));
		btnC.setBounds(27, 87, 361, 50);
		panelChoices.add(btnC);
		
		 btnD = new JButton("D.     ");
		 btnD.setRolloverEnabled(false);
		 btnD.setForeground(Color.WHITE);
		 btnD.setHorizontalTextPosition(AbstractButton.CENTER);
		 btnD.setOpaque(false);
		 btnD.setContentAreaFilled(false);
		 btnD.setBorderPainted(false);
		 btnD.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_22_4011160224.png")));
		 btnD.setFont(new Font("Georgia", Font.PLAIN, 15));
		btnD.setBounds(412, 87, 361, 50);
		panelChoices.add(btnD);
		
		lblquestionno = new JLabel("1");
		lblquestionno.setHorizontalAlignment(SwingConstants.CENTER);
		lblquestionno.setForeground(new Color(255, 250, 250));
		lblquestionno.setFont(new Font("Georgia", Font.BOLD, 35));
		lblquestionno.setBounds(379, 52, 46, 41);
		panelChoices.add(lblquestionno);
		
		 panel_2 = new JPanel();
		panel_2.setBackground(new Color(70, 130, 180,64));
		panel_2.setBounds(20, 11, 797, 63);
		panel.add(panel_2);
		panel_2.setLayout(null);
		
		 btnLL5050 = new JButton("");
		 btnLL5050.setRolloverEnabled(false);
		 btnLL5050.setForeground(Color.WHITE);
		 btnLL5050.setHorizontalTextPosition(AbstractButton.CENTER);
		 btnLL5050.setOpaque(false);
		 btnLL5050.setContentAreaFilled(false);
		 btnLL5050.setBorderPainted(false);
		 btnLL5050.setIcon(new ImageIcon("C:\\Users\\Adille\\Downloads\\imageedit_27_7266052596.png"));
		btnLL5050.setBounds(165, 0, 130, 63);
		panel_2.add(btnLL5050);
		btnLL5050.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnLL5050.setEnabled(false);
				if(CorrectAns.equals(btnA.getText())){
					btnA.setVisible(true);
					btnB.setVisible(false);
					btnC.setVisible(false);
					btnD.setVisible(false);
					
				}else if (CorrectAns.equals(btnB.getText())){
					btnB.setVisible(true);
					btnA.setVisible(false);
					btnC.setVisible(false);
					btnD.setVisible(false);
				}else if (CorrectAns.equals(btnC.getText())){
					btnC.setVisible(true);
					btnA.setVisible(false);
					btnB.setVisible(false);
					btnD.setVisible(false);
				}else if (CorrectAns.equals(btnD.getText())){
					btnD.setVisible(true);
					btnA.setVisible(false);
					btnB.setVisible(false);
					btnC.setVisible(false);
				}
				
				if(AnotherAns.equals(btnA.getText())){
					btnA.setVisible(true);
				}else if(AnotherAns.equals(btnB.getText())){
					btnB.setVisible(true);
				}else if(AnotherAns.equals(btnC.getText())){
					btnC.setVisible(true);
				}else if(AnotherAns.equals(btnD.getText())){
					btnD.setVisible(true);
				}
				repaint();
			}
		});
		
		 btnLLCall = new JButton("");
		 btnLLCall.setRolloverEnabled(false);
		 btnLLCall.setForeground(Color.WHITE);
		 btnLLCall.setHorizontalTextPosition(AbstractButton.CENTER);
		 btnLLCall.setOpaque(false);
		 btnLLCall.setContentAreaFilled(false);
		 btnLLCall.setBorderPainted(false);
		 btnLLCall.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_30_6104888644.png")));
		btnLLCall.setBounds(333, 0, 130, 63);
		panel_2.add(btnLLCall);
		btnLLCall.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtaPanel.setVisible(false);
				cafpanel.setVisible(true);
				btnLLCall.setEnabled(false);
				callanslabel.setText("My answer is "+CorrectAns);
				CallquestiontextArea.setText(currentquestion);
				callA.setText("A. "+listchoices.get(0));
				callB.setText("B. "+listchoices.get(1));
				callC.setText("C. "+listchoices.get(2));
				callD.setText("D. "+listchoices.get(3));
			}
		});
		
		btnLLAskAudience = new JButton("");
		btnLLAskAudience.setRolloverEnabled(false);
		btnLLAskAudience.setForeground(Color.WHITE);
		btnLLAskAudience.setHorizontalTextPosition(AbstractButton.CENTER);
		btnLLAskAudience.setOpaque(false);
		btnLLAskAudience.setContentAreaFilled(false);
		btnLLAskAudience.setBorderPainted(false);
		btnLLAskAudience.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_33_9034619878.png")));
		btnLLAskAudience.setBounds(496, 0, 130, 63);
		panel_2.add(btnLLAskAudience);
		btnLLAskAudience.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//btnLLAskAudience.setEnabled(false);
				SetAskAudience();
				cafpanel.setVisible(false);
				AtaPanel.setVisible(true);
				btnLLAskAudience.setEnabled(false);
			}

			private void SetAskAudience() {
				// TODO Auto-generated method stub
				
				Random rand = new Random();
				
				
				int A = rand.nextInt(97) + 1;
				int tempA = 100 - A;
				int B = rand.nextInt(tempA) + 1;
				int tempB = 100-(A+B);
				int C = rand.nextInt(tempB) + 1;


				
				int D = 100 - (A + B + C);
			
				int result = A + B + C + D;
				//System.out.println("A + B + C + D = " + result);
				
				percentA.setText(A+"%");
				percentB.setText(B+"%");
				percentC.setText(C+"%");
				percentD.setText(D+"%");
				progressBarA.setValue(A);
				progressBarB.setValue(B);
				progressBarC.setValue(C);
				progressBarD.setValue(D);
				progressBarA.setString(A+"%");
				progressBarB.setString(B+"%");
				progressBarC.setString(C+"%");
				progressBarD.setString(D+"%");
			}
		});
		
		repaint();
		JButton btnwalkaway = new JButton("");
		btnwalkaway.setRolloverEnabled(false);
		btnwalkaway.setOpaque(false);
		btnwalkaway.setForeground(Color.WHITE);
		btnwalkaway.setHorizontalTextPosition(AbstractButton.CENTER);
		btnwalkaway.setContentAreaFilled(false);
		btnwalkaway.setBorderPainted(false);
		btnwalkaway.setIcon(new ImageIcon(GameFrame.class.getResource("/images/imageedit_35_3877677090.png")));
		btnwalkaway.setBounds(10, 0, 102, 63);
		panel_2.add(btnwalkaway);
		btnwalkaway.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent arg0) {
				repaint();
				btnwalkaway.revalidate();
				 int reply = JOptionPane.showConfirmDialog(null, "Walk Away with "+accmoney+" pesos?", "Who wants to be a Millionaire", JOptionPane.YES_NO_OPTION);
				 repaint();   
				 if (reply == JOptionPane.YES_OPTION) {
					 repaint();
			          JOptionPane.showMessageDialog(null, "You Walk Away with "+accmoney+" pesos. Thank you for playing!");
			          Gameover();
			        }
			        else {
			        	
			           
			        }
				
			}

		});
		
		lblQuestion.setLineWrap(true);
		lblQuestion.setWrapStyleWord(true);
		lblQuestion.setOpaque(false);
		 
		 cafpanel = new JPanel();
		 cafpanel.setBounds(20, 83, 797, 181);
		 panel.add(cafpanel);
		 cafpanel.setLayout(null);
		 
		 label = new JLabel("New label");
		 label.setIcon(new ImageIcon(GameFrame.class.getResource("/images/wifi-calling-icon.png")));
		 label.setBounds(-23, 45, 146, 92);
		 cafpanel.add(label);
		 
		 label_1 = new JLabel("Hello...");
		 label_1.setFont(new Font("Georgia", Font.PLAIN, 15));
		 label_1.setBounds(496, 11, 162, 14);
		 cafpanel.add(label_1);
		 
		 label_2 = new JLabel("Help me answer this question");
		 label_2.setFont(new Font("Georgia", Font.PLAIN, 15));
		 label_2.setBounds(135, 30, 208, 14);
		 cafpanel.add(label_2);
		 
		 CallquestiontextArea = new JTextArea();
		 CallquestiontextArea.setLineWrap(true);
		 CallquestiontextArea.setWrapStyleWord(true);
		 CallquestiontextArea.setText("These are sets of programs, responsible for running the computer, controlling various operations of computer systems and management of computer resources.");
		 CallquestiontextArea.setFont(new Font("Georgia", Font.PLAIN, 15));
		 CallquestiontextArea.setBackground(SystemColor.menu);
		 CallquestiontextArea.setBounds(133, 45, 580, 43);
		 cafpanel.add(CallquestiontextArea);
		
		 callA = new JLabel("A");
		 callA.setFont(new Font("Georgia", Font.PLAIN, 15));
		 callA.setBounds(136, 91, 215, 14);
		 cafpanel.add(callA);
		 
		 callB = new JLabel("B");
		 callB.setFont(new Font("Georgia", Font.PLAIN, 15));
		 callB.setBounds(136, 106, 215, 14);
		 cafpanel.add(callB);
		 
		 callC = new JLabel("C");
		 callC.setFont(new Font("Georgia", Font.PLAIN, 15));
		 callC.setBounds(136, 123, 215, 14);
		 cafpanel.add(callC);
		 
		 callD = new JLabel("D");
		 callD.setFont(new Font("Georgia", Font.PLAIN, 15));
		 callD.setBounds(136, 139, 215, 14);
		 cafpanel.add(callD);
		 
		 callanslabel = new JLabel("My answer is chuchu....");
		 callanslabel.setFont(new Font("Georgia", Font.PLAIN, 15));
		 callanslabel.setBounds(496, 156, 261, 14);
		 cafpanel.add(callanslabel);
		 cafpanel.setVisible(false);
		 
		 
		 AtaPanel = new JPanel();
		 AtaPanel.setBounds(20, 83, 797, 181);
		 panel.add(AtaPanel);
		 AtaPanel.setBackground(new Color(70, 130, 180, 110));
		 AtaPanel.setLayout(null);
		 AtaPanel.setVisible(false);
		 
		 panel_5 = new JPanel();
		 panel_5.setLayout(null);
		 panel_5.setBackground(SystemColor.activeCaption);
		 panel_5.setBounds(669, 12, 70, 159);
		 AtaPanel.add(panel_5);
		 
		 percentA = new JLabel("100%");
		 percentA.setForeground(Color.WHITE);
		 percentA.setFont(new Font("Georgia", Font.PLAIN, 20));
		 percentA.setBounds(9, 4, 58, 34);
		 panel_5.add(percentA);
		 
		 percentB = new JLabel("100%");
		 percentB.setForeground(Color.WHITE);
		 percentB.setFont(new Font("Georgia", Font.PLAIN, 20));
		 percentB.setBounds(10, 44, 58, 34);
		 panel_5.add(percentB);
		 
		 percentC = new JLabel("100%");
		 percentC.setForeground(Color.WHITE);
		 percentC.setFont(new Font("Georgia", Font.PLAIN, 20));
		 percentC.setBounds(10, 81, 58, 34);
		 panel_5.add(percentC);
		 
		 percentD = new JLabel("100%");
		 percentD.setForeground(Color.WHITE);
		 percentD.setFont(new Font("Georgia", Font.PLAIN, 20));
		 percentD.setBounds(7, 120, 58, 34);
		 panel_5.add(percentD);
		 
		 panel_6 = new JPanel();
		 panel_6.setLayout(null);
		 panel_6.setBackground(SystemColor.activeCaption);
		 panel_6.setBounds(111, 11, 550, 159);
		 AtaPanel.add(panel_6);
		 
		 progressBarA = new JProgressBar();
		 progressBarA.setValue(50);
		 progressBarA.setStringPainted(true);
		 progressBarA.setString("50%");
		 progressBarA.setForeground(Color.RED);
		 progressBarA.setFont(new Font("Georgia", Font.PLAIN, 15));
		 progressBarA.setBounds(7, 5, 533, 34);
		 panel_6.add(progressBarA);
		 
		 progressBarB = new JProgressBar();
		 progressBarB.setValue(50);
		 progressBarB.setStringPainted(true);
		 progressBarB.setString("50%");
		 progressBarB.setForeground(Color.RED);
		 progressBarB.setFont(new Font("Georgia", Font.PLAIN, 15));
		 progressBarB.setBounds(8, 44, 533, 34);
		 panel_6.add(progressBarB);
		 
		 progressBarC = new JProgressBar();
		 progressBarC.setValue(50);
		 progressBarC.setStringPainted(true);
		 progressBarC.setString("50%");
		 progressBarC.setForeground(Color.RED);
		 progressBarC.setFont(new Font("Georgia", Font.PLAIN, 15));
		 progressBarC.setBounds(7, 83, 533, 34);
		 panel_6.add(progressBarC);
		 
		 progressBarD = new JProgressBar();
		 progressBarD.setValue(50);
		 progressBarD.setStringPainted(true);
		 progressBarD.setString("50%");
		 progressBarD.setForeground(Color.RED);
		 progressBarD.setFont(new Font("Georgia", Font.PLAIN, 15));
		 progressBarD.setBounds(7, 120, 534, 34);
		 panel_6.add(progressBarD);
		 
		 panel_4 = new JPanel();
		 panel_4.setBounds(50, 11, 55, 159);
		 AtaPanel.add(panel_4);
		 panel_4.setLayout(null);
		 panel_4.setBackground(SystemColor.activeCaption);
		 
		 ChoiceA = new JLabel("A");
		 ChoiceA.setHorizontalAlignment(SwingConstants.CENTER);
		 ChoiceA.setForeground(Color.WHITE);
		 ChoiceA.setFont(new Font("Georgia", Font.BOLD, 25));
		 ChoiceA.setBounds(14, 2, 31, 34);
		 panel_4.add(ChoiceA);
		 
		 ChoiceB = new JLabel("B");
		 ChoiceB.setHorizontalAlignment(SwingConstants.CENTER);
		 ChoiceB.setForeground(Color.WHITE);
		 ChoiceB.setFont(new Font("Georgia", Font.BOLD, 25));
		 ChoiceB.setBounds(0, 43, 58, 34);
		 panel_4.add(ChoiceB);
		 
		 ChoiceC = new JLabel("C");
		 ChoiceC.setHorizontalAlignment(SwingConstants.CENTER);
		 ChoiceC.setForeground(Color.WHITE);
		 ChoiceC.setFont(new Font("Georgia", Font.BOLD, 25));
		 ChoiceC.setBounds(0, 80, 58, 34);
		 panel_4.add(ChoiceC);
		 
		 ChoiceD = new JLabel("D");
		 ChoiceD.setHorizontalAlignment(SwingConstants.CENTER);
		 ChoiceD.setForeground(Color.WHITE);
		 ChoiceD.setFont(new Font("Georgia", Font.BOLD, 25));
		 ChoiceD.setBounds(0, 114, 58, 34);
		 panel_4.add(ChoiceD);
		
		 labelGameBackground = new JLabel("");
		 labelGameBackground.setIcon(new ImageIcon(GameFrame.class.getResource("/images/gamebackground2.jpg")));
		 labelGameBackground.setBounds(-410, -35, 1548, 698);
		 panel.add(labelGameBackground);
		setVisible(true);
		setResizable(false);
		setTitle("Who wants to be a millionaire CCIS Edition");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(170, 20, 1097, 692);
		
		RandomQuestion();
		repaint();
		LoadQuestions();
		btnA.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtaPanel.setVisible(false);
				cafpanel.setVisible(false);
				btnA.setVisible(true);
				btnB.setVisible(true);
				btnC.setVisible(true);
				btnD.setVisible(true);
				if(CheckAnswer(0)==true){
					moneyctr += 1;
					//System.out.println("Moneyctr -"+moneyctr);
					Moneytree(moneyctr);
					if(endctr==1){
						Gameover();
					}else {
						repaint();
					LoadQuestions();
					}
					
				}else {
					btnA.setBackground(Color.red);
					btnA.setForeground(Color.white);
					JOptionPane.showMessageDialog (null, "Game Over! You won: "+moneybank+". Thank you for playing.", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
					
					Gameover();
				}
				
				
			}
		});
		btnB.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtaPanel.setVisible(false);
				cafpanel.setVisible(false);
				btnA.setVisible(true);
				btnB.setVisible(true);
				btnC.setVisible(true);
				btnD.setVisible(true);
				if(CheckAnswer(1)==true){
					moneyctr += 1;
					//System.out.println("Moneyctr -"+moneyctr);
					Moneytree(moneyctr);
					if(endctr==1){
						Gameover();
					}else {
						//System.out.println("pasok1");
					repaint();
					LoadQuestions();
					//System.out.println("pasok2");
					}
					
				}else {
					btnB.setBackground(Color.red);
					btnB.setForeground(Color.white);
					JOptionPane.showMessageDialog (null, "Game Over! You won: "+moneybank+". Thank you for playing.", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
					Gameover();
				}
				
				
			}
		});
		btnC.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtaPanel.setVisible(false);
				cafpanel.setVisible(false);
				btnA.setVisible(true);
				btnB.setVisible(true);
				btnC.setVisible(true);
				btnD.setVisible(true);
				if(CheckAnswer(2)==true){
					moneyctr += 1;
					//System.out.println("Moneyctr -"+moneyctr);
					Moneytree(moneyctr);
					if(endctr==1){
						Gameover();
					}else {
						//System.out.println("pasok1");
					repaint();
					LoadQuestions();
					//System.out.println("pasok2");
					}
					
				}else {
					btnC.setBackground(Color.red);
					btnC.setForeground(Color.white);
					JOptionPane.showMessageDialog (null, "Game Over! You won: "+moneybank+". Thank you for playing.", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
					Gameover();
				}
				
				
			}
		});
		btnD.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AtaPanel.setVisible(false);
				cafpanel.setVisible(false);
				btnA.setVisible(true);
				btnB.setVisible(true);
				btnC.setVisible(true);
				btnD.setVisible(true);
				if(CheckAnswer(3)==true){
					moneyctr += 1;
					//System.out.println("Moneyctr -"+moneyctr);
					Moneytree(moneyctr);
					if(endctr==1){
						Gameover();
					}else {
						//System.out.println("pasok1");
					repaint();
					LoadQuestions();
					//System.out.println("pasok2");
					}
					
				}else {
					btnD.setBackground(Color.red);
					btnD.setForeground(Color.white);
					JOptionPane.showMessageDialog (null, "Game Over! You won: "+moneybank+".Thank you for playing.", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
					Gameover();
				}
				
				
			}

			
		});
		
	}
	
	private void Moneytree(int moneyctr2) {
		// TODO Auto-generated method stub
		
		 switch(moneyctr2) {
         case 1 :
        	 lblMoney1.setBackground(new Color(72, 61, 139));
        	 accmoney = 1000;
        	 questionno = "2";
        	 lblquestionno.setText(questionno);
            break;
         case 2 :
        	 lblMoney1.setBackground(new Color(70, 130, 180));
        	 lblMoney2.setBackground(new Color(72, 61, 139));
        	 accmoney = 3000;
        	 questionno = "3";
        	 lblquestionno.setText(questionno);
        	 break;
         case 3 :
        	 lblMoney2.setBackground(new Color(70, 130, 180));
        	 lblMoney3.setBackground(new Color(72, 61, 139));
        	 accmoney = 5000;
        	 questionno = "4";
        	 lblquestionno.setText(questionno);
            break;
         case 4 :
        	 lblMoney3.setBackground(new Color(70, 130, 180));
        	 lblMoney4.setBackground(new Color(72, 61, 139));
        	 accmoney = 10000;
        	 questionno = "5";
        	 lblquestionno.setText(questionno);
        	 break;
         case 5 :
        	 lblMoney4.setBackground(new Color(70, 130, 180));
        	 lblMoney5.setBackground(new Color(72, 61, 139));
        	 accmoney = 20000;
        	 moneybank = 20000;
        	 questionno = "6";
        	 lblquestionno.setText(questionno);
        	 JOptionPane.showMessageDialog (null, "Money Bank: 20,000", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
        	 
            break;
         case 6 :
        	 lblMoney5.setBackground(new Color(70, 130, 180));
        	 lblMoney6.setBackground(new Color(72, 61, 139));
        	 accmoney = 35000;
        	 questionno = "7";
        	 lblquestionno.setText(questionno);
             break;
         case 7 :
        	 lblMoney6.setBackground(new Color(70, 130, 180));
        	 lblMoney7.setBackground(new Color(72, 61, 139));
        	 accmoney = 50000;
        	 questionno = "8";
        	 lblquestionno.setText(questionno);
             break;
         case 8 :
        	 lblMoney7.setBackground(new Color(70, 130, 180));
        	 lblMoney8.setBackground(new Color(72, 61, 139));
        	 accmoney = 70000;
        	 questionno = "9";
        	 lblquestionno.setText(questionno);
             break;
         case 9 :
        	 lblMoney8.setBackground(new Color(70, 130, 180));
        	 lblMoney9.setBackground(new Color(72, 61, 139));
        	 accmoney = 100000;
        	 questionno = "10";
        	 lblquestionno.setText(questionno);
             break;
         case 10 :
        	 lblMoney9.setBackground(new Color(70, 130, 180));
        	 lblMoney10.setBackground(new Color(72, 61, 139));
        	 accmoney = 150000;
        	 moneybank = 150000;
        	 questionno = "11";
        	 lblquestionno.setText(questionno);
        	 JOptionPane.showMessageDialog (null, "Money Bank: 150,000", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
        	 
             break;
         case 11 :
        	 lblMoney10.setBackground(new Color(70, 130, 180));
        	 lblMoney11.setBackground(new Color(72, 61, 139));
        	 accmoney = 250000;
        	 questionno = "12";
        	 lblquestionno.setText(questionno);
        	
             break;
         case 12 :
        	 lblMoney11.setBackground(new Color(70, 130, 180));
        	 lblMoney12.setBackground(new Color(72, 61, 139));
        	 accmoney = 400000;
        	 questionno = "13";
        	 lblquestionno.setText(questionno);
             break;
         case 13 :
        	 lblMoney12.setBackground(new Color(70, 130, 180));
        	 lblMoney13.setBackground(new Color(72, 61, 139));
        	 accmoney = 600000;
        	 questionno = "14";
        	 lblquestionno.setText(questionno);
             break;
         case 14 :
        	 lblMoney13.setBackground(new Color(70, 130, 180));
        	 lblMoney14.setBackground(new Color(72, 61, 139));
        	 accmoney = 1000000;
        	 questionno = "15";
        	 lblquestionno.setText(questionno);
             break;
         case 15 :
        	 lblMoney14.setBackground(new Color(70, 130, 180));
        	 lblMoney15.setBackground(new Color(72, 61, 139));
        	 accmoney = 2000000;
        	 moneybank = 2000000;
        	 questionno = "";
        	 lblquestionno.setText(questionno);
        	// System.out.println("case 15");
        	 endctr=1;
        	 JOptionPane.showMessageDialog (null, "You won! Money Bank: 2,000,000. Thank you for playing.", "Who wants to be a Millionaire", JOptionPane.INFORMATION_MESSAGE);
        	 
             break;
         
         default :
            System.out.println("Game over");
      }
	}

	private void Gameover() {
		// TODO Auto-generated method stub
		list.removeAll(list);
		listchoices.removeAll(listchoices);
		setVisible(false);
		WelcomeScreen wc = new WelcomeScreen();
		wc.setVisible(true);
	}
	public boolean CheckAnswer(int checkint){
		boolean isCheck = false;
		try {
			
			c = new Connect();
			
			String query = "SELECT * FROM questionbank_table where Question_Id = ?;";
			
			c.pst = c.con.prepareStatement(query);
			c.pst.setString(1, Integer.toString(list.get(0)));
			c.pst.execute();
			c.rs = c.pst.getResultSet();
			
			while(c.rs.next())
			{
				if(c.rs.getString("A1").equals(listchoices.get(checkint))){
					//System.out.println(c.rs.getString("A1")+" = "+listchoices.get(checkint));
					isCheck = true;
				}else {
					isCheck = false;
				}
			}
			//System.out.println(list.get(0));
			
			//System.out.println(list+" before remove");
			list.remove(0);
			listchoices.removeAll(listchoices);
			CorrectAns = null;
			
			c.rs.close();
			c.pst.close(); 	
			c.con.close();
			
		}catch(SQLException e1) {
			e1.printStackTrace();
		}
		
		return isCheck;
		
		
		
	}
	public void RandomQuestion(){
		try {
			
			c = new Connect();
			
			String query = "SELECT * FROM questionbank_table;";
			
			
			c.pst = c.con.prepareStatement(query);
			c.pst.execute();
			c.rs = c.pst.getResultSet();

			   list = new ArrayList<Integer>();
			while(c.rs.next())
			{

		                list.add(c.rs.getInt("Question_Id"));
		                //System.out.println(list);
		       
			}
			 Collections.shuffle(list);
		   
		            //System.out.println(list.get(0));
		      
			
			c.rs.close();
			c.pst.close(); 	
			c.con.close();
		}catch(SQLException e1) {
			e1.printStackTrace();
		}
	}
	public void RandomChoices(Integer integer) {
		// TODO Auto-generated method stub
try {
			
			c = new Connect();
			
			String query = "SELECT * FROM questionbank_table where Question_Id = ?;";
			
			
			c.pst = c.con.prepareStatement(query);
			c.pst.setString(1, Integer.toString(integer));
			c.pst.execute();
			c.rs = c.pst.getResultSet();

			listchoices = new ArrayList<String>();  
			while(c.rs.next())
			{

					CorrectAns = c.rs.getString("A1");
					AnotherAns = c.rs.getString("A2");
					listchoices.add(c.rs.getString("A1"));
					listchoices.add(c.rs.getString("A2"));
					listchoices.add(c.rs.getString("A3"));
					listchoices.add(c.rs.getString("A4"));
					
		       
			}
			Collections.shuffle(listchoices); 
			//System.out.println("Listchoices -"+listchoices);
			//System.out.println("Correct ANswer "+CorrectAns);
			
			c.rs.close();
			c.pst.close(); 	
			c.con.close();
		}catch(SQLException e1) {
			e1.printStackTrace();
		}
	}
	public void LoadQuestions(){
 
		try {
			
			c = new Connect();
			
			String query = "SELECT * FROM questionbank_table where Question_Id = ?;";
			
			
			c.pst = c.con.prepareStatement(query);
			c.pst.setString(1, Integer.toString(list.get(0)));
			c.pst.execute();
			c.rs = c.pst.getResultSet();
			
			
			
			
			while(c.rs.next())
			{
				currentquestion=c.rs.getString("Question");
				lblQuestion.setText(c.rs.getString("Question"));
				
			}
			RandomChoices(list.get(0));
			btnA.setText(listchoices.get(0));
			btnB.setText(listchoices.get(1));
			btnC.setText(listchoices.get(2));
			btnD.setText(listchoices.get(3));
			
			
			c.rs.close();
			c.pst.close(); 	
			c.con.close();
		}catch(SQLException e1) {
			e1.printStackTrace();
		}
 
   
	}
}
