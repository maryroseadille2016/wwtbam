CREATE DATABASE  IF NOT EXISTS `wwtbam`;
USE `wwtbam`;


CREATE TABLE `wwtbam`.`questionbank_table` (
  `Question_Id` INT NOT NULL AUTO_INCREMENT,
  `Question` VARCHAR(250) NOT NULL,
  `A1` VARCHAR(150) NOT NULL,
  `A2` VARCHAR(150) NOT NULL,
  `A3` VARCHAR(150) NOT NULL,
  `A4` VARCHAR(150) NOT NULL,
  PRIMARY KEY (`Question_Id`),
  UNIQUE INDEX `Question_Id_UNIQUE` (`Question_Id` ASC));




INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('1', 'It is a device that transforms data into meaningful information.', 'Computer', 'Mouse', 'Router', 'Monitor');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('2', 'Refers to the physical components of your computer such as the system unit, mouse, keyboard, monitor etc.', 'Hardware', 'Software', 'Peopleware', 'Internetware');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('3', 'An electronic tool that allows information to be input, processed, and output', 'Computer', 'Operating system', 'Motherboard', 'CPU');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('4', 'A worldwide network of computers', 'Internet', 'CPU', 'RAM', 'Network');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('5', 'The brain of the computer. This part does the calculation, moving and processing of information', 'CPU', 'RAM', 'Motherboard', 'Hard Drive');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('6', 'These are the systems on the lower end of the size of the size scale.', 'Microcomputer  ', 'Personal computers ', 'Minicomputers', 'Mainframes');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('7', 'These systems are the largest, fastest and most expensive computers in the world.', 'Supercomputers', 'Microcomputer  ', 'Personal computers ', 'Mainframes');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('8', 'It is a device that merges functionality of phones, PDAs, cameras, camcorders and computers.', 'Smartphone', 'Mobile phone', 'PDA-Personal Digital Assistant (PALM)', 'Telephone');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('9', 'What is the computer\'s basic circuit, to which all computer components are connected, directly or indirectly through a system bus?', 'Motherboard', 'Central Processing Unit ', 'Cache', 'System Unit');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('10', 'It is a type of permanent, internal memory that is used solely for reading.', 'ROM (Read Only Memory)', 'RAM (Random Access Memory)', 'Hard Disk Drive (HDD)', 'Floppy Disk Drive');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('11', 'It is responsible for image processing and displaying it on a monitor. Image quality depends on the strength of these components.', 'Graphics card', 'Soundcard', 'Graphics processor', 'Modem');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('12', 'What is the successor of DVD, which comes in different capacities, depending on how many layers it has and the capacity of each layer.', 'Blu-ray disc (BD)', 'Memory card', 'DVD (Digital Versatile Disc)', 'CD (Compact Disc)');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('13', 'The process of input, output, processing and storage is performed under the supervision of a unit called?', 'Control Unit (CU)', 'Memory Unit', 'Output', 'Arithmetic Logic Unit (ALU)');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('14', 'These are sets of programs, responsible for running the computer, controlling various operations of computer systems and management of computer resources.', 'System Software', 'Application Software', 'General Software', 'Computer Software');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('15', 'Which of the following Operating Systems is based on Linux?', 'Fedora', 'Panther', 'Cheetah', 'Vista');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('16', 'According to Flynn and McHoes, the following are components of an operating system EXCEPT', 'User Command Manager', 'File Manager', 'Device Manager', 'Memory Manager');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('17', 'Part of a computer that allows a user to put information into the computer', 'Input Device', 'Operating System', 'Software', 'Output Device');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('18', 'A small picture that represents a folder, program or other things', 'Icon', 'Desktop', 'Graphic', 'Image');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('19', 'A name for the short term memory of the computer that is lost when the computer is turned off', 'RAM', 'Hardware', 'CPU', 'Processor');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('20', 'Parts of a computer that allow the user to see or hear information that comes out from the computer', 'Output Device', 'Input Device', 'Software', 'Operating System');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('21', 'This person created a machine called The Analytical Engine.  His ideas were some of the first that led to the creation of computers.', 'Charles Babbage', 'Simon Konrad', 'John Lovelace', 'William Howard');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('22', 'This invention was 1,000 times faster than any machine built before it.  It was so big that it could fill up a house.', 'ENIAC', 'Windows', 'Apple 1', 'Z3');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('23', 'Steve Jobs and Steve Wozniak built their first computer using a wooden box.  Their company has grown and is still around today.  The name of the company is:', 'Apple', 'Microsoft', 'Linux', 'Ubuntu');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('24', 'This person  is now known as the first computer programmer.  ', 'Ada Lovelace', 'Konrad Apple', 'Charles Babbage', 'Steve Jobs');
INSERT INTO `wwtbam`.`questionbank_table` (`Question_Id`, `Question`, `A1`, `A2`, `A3`, `A4`) VALUES ('25', 'This man is known for starting the company Microsoft back in the year 1975.  Since then, he has become one of the richest people in the world.  His name is: ', 'Bill Gates', 'Steve Jobs', 'Konrad Zuse', 'Charles Babbage');



UPDATE `wwtbam`.`questionbank_table` SET `A1`='Read Only Memory', `A2`='Random Access Memory', `A3`='Hard Disk Drive' WHERE `Question_Id`='10';
UPDATE `wwtbam`.`questionbank_table` SET `A3`=' Digital Versatile Disc' WHERE `Question_Id`='12';
UPDATE `wwtbam`.`questionbank_table` SET `A4`='Arithmetic Logic Unit' WHERE `Question_Id`='13';
UPDATE `wwtbam`.`questionbank_table` SET `A3`='PDA-Personal Digital Assistant' WHERE `Question_Id`='8';
